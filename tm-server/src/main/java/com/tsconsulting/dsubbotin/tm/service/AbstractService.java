package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IService;
import com.tsconsulting.dsubbotin.tm.dto.AbstractEntityDTO;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntityDTO> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

}
