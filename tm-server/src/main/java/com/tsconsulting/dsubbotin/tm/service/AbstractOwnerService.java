package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IOwnerService;
import com.tsconsulting.dsubbotin.tm.dto.AbstractOwnerEntityDTO;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntityDTO> extends AbstractService<E> implements IOwnerService<E> {

    public AbstractOwnerService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

}
