package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO sessions" +
            " (id, date, signature, user_id)" +
            " VALUES (#{session.id}, #{session.date}, #{session.signature}, #{session.userId});")
    void open(@NotNull @Param("session") final SessionDTO session);

    @Update("DELETE FROM sessions WHERE id = #{session.id};")
    int close(@NotNull @Param("session") final SessionDTO session);

    @Select("SELECT * FROM sessions WHERE id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    SessionDTO contains(@NotNull @Param("id") final String id);

    @Delete("DELETE FROM sessions WHERE id = #{session.id}")
    void remove(@NotNull @Param("session") SessionDTO session);

    @Delete("DELETE FROM sessions")
    void clear();

    @NotNull
    @Select("SELECT * FROM sessions")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<SessionDTO> findAll();

    @NotNull
    @Select("SELECT * FROM sessions WHERE id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    SessionDTO findById(@NotNull @Param("id") String id);

    @NotNull
    @Select("SELECT * FROM sessions LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    SessionDTO findByIndex(@Param("index") int index);

    @Delete("DELETE FROM sessions WHERE id = #{id}")
    void removeById(@NotNull @Param("id") String id);

}
