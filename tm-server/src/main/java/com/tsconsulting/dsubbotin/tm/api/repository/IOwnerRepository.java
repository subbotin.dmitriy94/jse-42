package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.dto.AbstractOwnerEntityDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntityDTO> extends IRepository<E> {

    boolean existById(@NotNull String userId, @NotNull String id);

    void remove(@NotNull String userId, @NotNull E entity) throws AbstractException, SQLException;

    void clear(@NotNull String userId) throws SQLException;

    @NotNull
    List<E> findAll(@NotNull String userId) throws AbstractException, SQLException;

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator) throws AbstractException, SQLException;

    @NotNull
    E findById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

    @NotNull
    E findByIndex(@NotNull String userId, int index) throws AbstractException, SQLException;

    void removeById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

    void removeByIndex(@NotNull String userId, int index) throws AbstractException, SQLException;

}