package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownSortException;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractOwnerService<TaskDTO> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    public ITaskRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(ITaskRepository.class);
    }

    @Override
    public void clear() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.clear();
            session.commit();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final List<TaskDTO> tasks = taskRepository.findAll();
            if (tasks == null) throw new TaskNotFoundException();
            return tasks;
        }
    }

    @Override
    public boolean existById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final TaskDTO task = taskRepository.existById(userId, id);
            return task != null;
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final TaskDTO task) {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.remove(userId, task);
            session.commit();
        }
    }

    @Override
    @NotNull
    public List<TaskDTO> findAll(
            @NotNull final String userId,
            @Nullable final String sort
    ) throws AbstractException {
        try {
            @NotNull final Sort sortType = EnumerationUtil.parseSort(sort);
            @NotNull final Comparator<TaskDTO> comparator = sortType.getComparator();
            return findAll(userId, comparator);
        } catch (UnknownSortException exception) {
            try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
                @NotNull final ITaskRepository taskRepository = getRepository(session);
                @Nullable final List<TaskDTO> tasks = taskRepository.findAllByUserId(userId);
                if (tasks == null) throw new TaskNotFoundException();
                return tasks;
            }
        }
    }

    @Override
    @NotNull
    public List<TaskDTO> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<TaskDTO> comparator
    ) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final List<TaskDTO> tasks = taskRepository.findAllByUserId(userId);
            if (tasks == null) throw new TaskNotFoundException();
            tasks.sort(comparator);
            return tasks;
        }
    }

    @Override
    @NotNull
    public TaskDTO findById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final TaskDTO task = taskRepository.findByUserIdAndTaskId(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Override
    @NotNull
    public TaskDTO findByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final TaskDTO task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Override
    public void removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.removeByUserIdAndTaskId(userId, id);
            session.commit();
        }
    }

    @Override
    public void removeByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final TaskDTO task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            taskRepository.removeByUserIdAndTaskId(userId, task.getId());
        }
    }

    @NotNull
    @Override
    public TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkName(name);
        if (EmptyUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.add(task);
            session.commit();
        }
        return task;
    }

    @Override
    public void addAll(@NotNull List<TaskDTO> tasks) {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            for (@NotNull final TaskDTO task : tasks) taskRepository.add(task);
            session.commit();
        }
    }

    @Override
    public void clear(@NotNull final String userId) {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.clearById(userId);
            session.commit();
        }
    }

    @Override
    @NotNull
    public TaskDTO findByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable TaskDTO task = taskRepository.findByName(userId, name);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Override
    public void removeByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.removeByName(userId, name);
            session.commit();
        }
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkId(id);
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.updateById(userId, id, name, description);
            session.commit();
        }
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkIndex(index);
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @NotNull final String id = findByIndex(userId, index).getId();
            taskRepository.updateByIndex(userId, id, name, description);
            session.commit();
        }
    }

    @Override
    public void startById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.startById(userId, id, Status.IN_PROGRESS.toString());
            session.commit();
        }
    }

    @Override
    public void startByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @NotNull final String id = findByIndex(userId, index).getId();
            taskRepository.startByIndex(userId, id, Status.IN_PROGRESS.toString());
            session.commit();
        }
    }

    @Override
    public void startByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.startByName(userId, name, Status.IN_PROGRESS.toString());
            session.commit();
        }
    }

    @Override
    public void finishById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.finishById(userId, id, Status.COMPLETED.toString());
            session.commit();
        }
    }

    @Override
    public void finishByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @NotNull final String id = findByIndex(userId, index).getId();
            taskRepository.finishByIndex(userId, id, Status.COMPLETED.toString());
            session.commit();
        }
    }

    @Override
    public void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.finishByName(userId, name, Status.COMPLETED.toString());
            session.commit();
        }
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.updateStatusById(userId, id, status.toString());
            session.commit();
        }
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @NotNull final String id = findByIndex(userId, index).getId();
            taskRepository.updateStatusByIndex(userId, id, status.toString());
            session.commit();
        }
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.updateStatusByName(userId, name, status.toString());
            session.commit();
        }
    }

    private void checkName(@NotNull final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

}