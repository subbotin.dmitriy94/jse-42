package com.tsconsulting.dsubbotin.tm.exception.system;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class DatabaseOperationException extends AbstractException {

    public DatabaseOperationException() {
        super("Database operation exception!");
    }

}
