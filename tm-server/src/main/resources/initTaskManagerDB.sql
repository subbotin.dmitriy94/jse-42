CREATE TABLE public.users (
  id VARCHAR(255) NOT NULL,
  login VARCHAR(255) NOT NULL,
  password_hash VARCHAR(255) NOT NULL,
  email VARCHAR(255),
  role VARCHAR(255) NOT NULL,
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  middle_name VARCHAR(255),
  locked BOOLEAN DEFAULT false NOT NULL,
  CONSTRAINT users_email_key UNIQUE(email),
  CONSTRAINT users_login_key UNIQUE(login),
  CONSTRAINT users_pkey PRIMARY KEY(id)
) ;


ALTER TABLE public.users
  OWNER TO postgres;
  

CREATE TABLE public.projects (
  id VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255),
  status VARCHAR(255) DEFAULT 'NOT_STARTED'::character varying NOT NULL,
  create_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  start_date TIMESTAMP(0) WITHOUT TIME ZONE,
  user_id VARCHAR(255) NOT NULL,
  CONSTRAINT projects_pkey PRIMARY KEY(id),
  CONSTRAINT projects_fk FOREIGN KEY (user_id)
    REFERENCES public.users(id)
    MATCH FULL
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    NOT DEFERRABLE
) ;


ALTER TABLE public.projects
  OWNER TO postgres;
  
  
CREATE TABLE public.tasks (
  id VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255),
  status VARCHAR(255) DEFAULT 'NOT_STARTED'::character varying NOT NULL,
  create_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  start_date TIMESTAMP(0) WITHOUT TIME ZONE,
  project_id VARCHAR(255),
  user_id VARCHAR(255) NOT NULL,
  CONSTRAINT tasks_pkey PRIMARY KEY(id),
  CONSTRAINT tasks_fk FOREIGN KEY (user_id)
    REFERENCES public.users(id)
    MATCH FULL
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    NOT DEFERRABLE,
  CONSTRAINT tasks_fk1 FOREIGN KEY (project_id)
    REFERENCES public.projects(id)
    MATCH FULL
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    NOT DEFERRABLE
) ;


ALTER TABLE public.tasks
  OWNER TO postgres;
  
  
CREATE TABLE public.sessions (
  id VARCHAR(255) NOT NULL,
  date TIMESTAMP(0) WITHOUT TIME ZONE,
  signature VARCHAR(255),
  user_id VARCHAR(255) NOT NULL,
  CONSTRAINT sessions_pkey PRIMARY KEY(id),
  CONSTRAINT sessions_fk FOREIGN KEY (user_id)
    REFERENCES public.users(id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    NOT DEFERRABLE
) ;


ALTER TABLE public.sessions
  OWNER TO postgres;