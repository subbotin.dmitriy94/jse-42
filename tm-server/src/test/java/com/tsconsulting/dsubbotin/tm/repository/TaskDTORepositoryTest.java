package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.dto.ProjectDTO;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public final class TaskDTORepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final TaskDTO task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTask";

    @NotNull
    private final ProjectDTO project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String userId;

    @NotNull
    private final SqlSession sqlSession;

    public TaskDTORepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService sqlSessionService = new ConnectionService(propertyService);
        sqlSession = sqlSessionService.getSqlSession();
        taskRepository = sqlSession.getMapper(ITaskRepository.class);
        userRepository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final UserDTO user = new UserDTO();
        userId = user.getId();
        user.setLogin("guest");
        user.setPasswordHash(HashUtil.salt(3, "qwe", "guest"));
        userRepository.add(user);
        task = new TaskDTO();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setDescription(taskDescription);
        projectRepository = sqlSession.getMapper(IProjectRepository.class);
        project = new ProjectDTO();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName("Project");
        sqlSession.commit();
    }

    @Before
    public void initializeTest() {
        taskRepository.add(task);
        projectRepository.add(project);
        sqlSession.commit();
    }

    @Test
    public void findTask() {
        checkTask(taskRepository.findByName(userId, taskName));
        checkTask(taskRepository.findById(taskId));
        checkTask(taskRepository.findByUserIdAndTaskId(userId, taskId));
        checkTask(taskRepository.findByIndex(userId, 0));
    }

    private void checkTask(@Nullable final TaskDTO foundTask) {
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(task.getId(), foundTask.getId());
        Assert.assertEquals(task.getName(), foundTask.getName());
        Assert.assertEquals(task.getDescription(), foundTask.getDescription());
        Assert.assertEquals(task.getUserId(), foundTask.getUserId());
        Assert.assertEquals(task.getStartDate(), foundTask.getStartDate());
    }

    @Test
    public void removeByName() {
        Assert.assertNotNull(task);
        taskRepository.removeByName(userId, taskName);
        sqlSession.commit();
        Assert.assertNull(taskRepository.findByName(userId, taskName));
    }

    @Test
    public void updateById() {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskRepository.updateById(userId, taskId, newTaskName, newTaskDescription);
        sqlSession.commit();
        @Nullable final TaskDTO updatedTask = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(newTaskName, updatedTask.getName());
        Assert.assertEquals(newTaskDescription, updatedTask.getDescription());
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskRepository.updateByIndex(userId, taskId, newTaskName, newTaskDescription);
        sqlSession.commit();
        @Nullable final TaskDTO updatedTask = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(newTaskName, updatedTask.getName());
        Assert.assertEquals(newTaskDescription, updatedTask.getDescription());
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
    }

    @Test
    public void startById() {
        taskRepository.startById(userId, taskId, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        @Nullable final TaskDTO task = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() {
        taskRepository.startByIndex(userId, taskId, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        @Nullable final TaskDTO task = taskRepository.findByIndex(userId, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() {
        taskRepository.startByName(userId, taskName, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        @Nullable final TaskDTO task = taskRepository.findByName(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() {
        taskRepository.finishById(userId, taskId, Status.COMPLETED.toString());
        sqlSession.commit();
        @Nullable final TaskDTO task = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() {
        taskRepository.finishByIndex(userId, taskId, Status.COMPLETED.toString());
        sqlSession.commit();
        @Nullable final TaskDTO task = taskRepository.findByIndex(userId, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() {
        taskRepository.finishByName(userId, taskName, Status.COMPLETED.toString());
        sqlSession.commit();
        @Nullable final TaskDTO task = taskRepository.findByName(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() {
        taskRepository.updateStatusById(userId, taskId, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        @Nullable TaskDTO task = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
        taskRepository.updateStatusById(userId, taskId, Status.COMPLETED.toString());
        sqlSession.commit();
        task = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.COMPLETED);
        taskRepository.updateStatusById(userId, taskId, Status.NOT_STARTED.toString());
        sqlSession.commit();
        task = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() {
        taskRepository.updateStatusByIndex(userId, taskId, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        @Nullable TaskDTO task = taskRepository.findByIndex(userId, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
        taskRepository.updateStatusByIndex(userId, taskId, Status.COMPLETED.toString());
        sqlSession.commit();
        task = taskRepository.findByIndex(userId, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.COMPLETED);
        taskRepository.updateStatusByIndex(userId, taskId, Status.NOT_STARTED.toString());
        sqlSession.commit();
        task = taskRepository.findByIndex(userId, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() {
        taskRepository.updateStatusByName(userId, taskName, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        @Nullable TaskDTO task = taskRepository.findByName(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
        taskRepository.updateStatusByName(userId, taskName, Status.COMPLETED.toString());
        sqlSession.commit();
        task = taskRepository.findByName(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.COMPLETED);
        taskRepository.updateStatusByName(userId, taskName, Status.NOT_STARTED.toString());
        sqlSession.commit();
        task = taskRepository.findByName(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void bindTaskToProjectById() {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        @Nullable final TaskDTO task = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    public void unbindTaskById() {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        @Nullable TaskDTO task = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(projectId, task.getProjectId());
        taskRepository.unbindTaskById(userId, taskId);
        sqlSession.commit();
        task = taskRepository.findByUserIdAndTaskId(userId, taskId);
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void findAllByProjectId() {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        task.setProjectId(projectId);
        sqlSession.commit();
        @Nullable final List<TaskDTO> taskList = taskRepository.findAllByProjectId(userId, projectId);
        Assert.assertNotNull(taskList);
        @NotNull final TaskDTO tmpTask = taskList.get(0);
        Assert.assertEquals(task.getId(), tmpTask.getId());
        Assert.assertEquals(task.getName(), tmpTask.getName());
        Assert.assertEquals(task.getDescription(), tmpTask.getDescription());
        Assert.assertEquals(task.getProjectId(), tmpTask.getProjectId());
    }

    @Test
    public void removeAllTaskByProjectId() {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        @NotNull final TaskDTO task1 = new TaskDTO();
        task1.setName("Task1");
        task1.setUserId(userId);
        taskRepository.add(task1);
        taskRepository.bindTaskToProjectById(userId, projectId, task1.getId());
        sqlSession.commit();
        @Nullable final List<TaskDTO> taskList = taskRepository.findAllByProjectId(userId, projectId);
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        sqlSession.commit();
    }

    @After
    public void finalizeTest() {
        taskRepository.clearById(userId);
        projectRepository.clearById(userId);
        userRepository.removeById(userId);
        sqlSession.commit();
        sqlSession.close();
    }

}
