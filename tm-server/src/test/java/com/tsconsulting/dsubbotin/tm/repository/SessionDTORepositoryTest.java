package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public final class SessionDTORepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final SessionDTO session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final SqlSession sqlSession;

    public SessionDTORepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService sessionService = new ConnectionService(propertyService);
        sqlSession = sessionService.getSqlSession();
        userRepository = sqlSession.getMapper(IUserRepository.class);

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("guest");
        @NotNull final String password = "guest";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(iteration, secret, password));
        userRepository.add(user);
        sqlSession.commit();
        sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        session = new SessionDTO();
        sessionId = session.getId();
        session.setUserId(user.getId());
        session.setDate(new Date());
    }

    @Test
    public void openClose() {
        sessionRepository.open(session);
        sqlSession.commit();
        @NotNull final SessionDTO foundSession = sessionRepository.findById(sessionId);
        Assert.assertEquals(session.getId(), foundSession.getId());
        Assert.assertEquals(session.getUserId(), foundSession.getUserId());
        Assert.assertEquals(session.getSignature(), foundSession.getSignature());
        sessionRepository.close(session);
        sqlSession.commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void contains() {
        sessionRepository.open(session);
        sqlSession.commit();
        Assert.assertNotNull(sessionRepository.contains(sessionId));
    }

    @After
    public void finalizeTest() {
        sessionRepository.clear();
        userRepository.removeById(session.getUserId());
        sqlSession.commit();
    }

}
