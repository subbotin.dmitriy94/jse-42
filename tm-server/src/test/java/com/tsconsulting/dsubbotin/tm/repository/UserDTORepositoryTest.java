package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public final class UserDTORepositoryTest {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final SqlSession sqlSession;

    public UserDTORepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
        userRepository = sqlSession.getMapper(IUserRepository.class);
        user = new UserDTO();
        user.setLogin(userLogin);
        @NotNull final String password = "userTest";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(iteration, secret, password));
    }

    @Before
    public void initializeTest() {
        userRepository.add(user);
        sqlSession.commit();
    }

    @Test
    public void findByLogin() {
        @Nullable final UserDTO foundUser = userRepository.findByLogin(userLogin);
        Assert.assertNotNull(foundUser);
        Assert.assertEquals(user.getId(), foundUser.getId());
        Assert.assertEquals(user.getLogin(), foundUser.getLogin());
        Assert.assertEquals(user.getPasswordHash(), foundUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), foundUser.getRole());
    }

    @Test
    public void removeByLogin() {
        userRepository.removeByLogin(userLogin);
        sqlSession.commit();
        Assert.assertNull(userRepository.findByLogin(userLogin));
    }

    @Test
    public void isLogin() {
        Assert.assertNotNull(userRepository.isLogin(userLogin));
    }

    @After
    public void finalizeTest() {
        userRepository.removeById(user.getId());
        sqlSession.commit();
    }

}
