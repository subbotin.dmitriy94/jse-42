package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public final class TaskDTOServiceTest {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectTaskService projectTaskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "taskNameTest";

    @NotNull
    private final String taskDescription = "taskDescriptionTest";

    @NotNull
    private final String userId;

    @Nullable
    private String projectId;

    public TaskDTOServiceTest() throws AbstractException {
        @NotNull final PropertyService propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final LogService logService = new LogService();
        userService = new UserService(connectionService, logService);
        taskService = new TaskService(connectionService, logService);
        projectService = new ProjectService(connectionService, logService);
        projectTaskService = new ProjectTaskService(connectionService, logService);
        userId = userService.create("test", "test").getId();
        projectId = projectService.create(userId, "projectTest", "projectTest").getId();
        taskId = taskService.create(userId, taskName, taskDescription).getId();
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        @NotNull final TaskDTO newTask = taskService.create(userId, newTaskName, newTaskDescription);
        Assert.assertNotNull(taskService.findByName(userId, taskName));
        Assert.assertEquals(newTask.getName(), newTaskName);
        Assert.assertEquals(newTask.getDescription(), newTaskDescription);
        taskService.removeById(userId, newTask.getId());
    }

    @Test
    public void findByName() throws AbstractException {
        @NotNull final TaskDTO task = taskService.findByName(userId, taskName);
        Assert.assertEquals(task.getName(), taskName);
        Assert.assertEquals(task.getDescription(), taskDescription);
        Assert.assertEquals(task.getUserId(), userId);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskService.updateById(userId, taskId, newTaskName, newTaskDescription);
        @NotNull final TaskDTO tempTask = taskService.findByName(userId, newTaskName);
        Assert.assertNotEquals(tempTask.getName(), taskName);
        Assert.assertNotEquals(tempTask.getDescription(), taskDescription);
        Assert.assertEquals(tempTask.getName(), newTaskName);
        Assert.assertEquals(tempTask.getDescription(), newTaskDescription);
        Assert.assertEquals(tempTask.getId(), taskId);
        Assert.assertEquals(tempTask.getUserId(), userId);
    }

    @Test
    public void updateByIndex() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskService.updateByIndex(userId, 0, newTaskName, newTaskDescription);
        @NotNull final TaskDTO tempTask = taskService.findByName(userId, newTaskName);
        Assert.assertNotEquals(tempTask.getName(), taskName);
        Assert.assertNotEquals(tempTask.getDescription(), taskDescription);
        Assert.assertEquals(tempTask.getName(), newTaskName);
        Assert.assertEquals(tempTask.getDescription(), newTaskDescription);
        Assert.assertEquals(tempTask.getId(), taskId);
        Assert.assertEquals(tempTask.getUserId(), userId);
    }

    @Test
    public void startById() throws AbstractException {
        @NotNull final TaskDTO tempTask = taskService.findById(userId, taskId);
        Assert.assertNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startById(userId, taskId);
        @NotNull final TaskDTO updTempTask = taskService.findById(userId, taskId);
        Assert.assertNotNull(updTempTask.getStartDate());
        Assert.assertEquals(updTempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        @NotNull final TaskDTO tempTask = taskService.findByIndex(userId, 0);
        Assert.assertNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startByIndex(userId, 0);
        @NotNull final TaskDTO updTempTask = taskService.findByIndex(userId, 0);
        Assert.assertNotNull(updTempTask.getStartDate());
        Assert.assertEquals(updTempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        @NotNull final TaskDTO tempTask = taskService.findByName(userId, taskName);
        Assert.assertNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startByName(userId, taskName);
        @NotNull final TaskDTO updTempTask = taskService.findByName(userId, taskName);
        Assert.assertNotNull(updTempTask.getStartDate());
        Assert.assertEquals(updTempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException {
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.NOT_STARTED);
        taskService.finishById(userId, taskId);
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        Assert.assertEquals(taskService.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
        taskService.finishByIndex(userId, 0);
        Assert.assertEquals(taskService.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException {
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.NOT_STARTED);
        taskService.finishByName(userId, taskName);
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.NOT_STARTED);
        taskService.updateStatusById(userId, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.IN_PROGRESS);
        taskService.updateStatusById(userId, taskId, Status.COMPLETED);
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.COMPLETED);
        taskService.updateStatusById(userId, taskId, Status.NOT_STARTED);
        Assert.assertEquals(taskService.findById(userId, taskId).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        Assert.assertEquals(taskService.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
        taskService.updateStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(taskService.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
        taskService.updateStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(taskService.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
        taskService.updateStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(taskService.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.NOT_STARTED);
        taskService.updateStatusByName(userId, taskName, Status.IN_PROGRESS);
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.IN_PROGRESS);
        taskService.updateStatusByName(userId, taskName, Status.COMPLETED);
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.COMPLETED);
        taskService.updateStatusByName(userId, taskName, Status.NOT_STARTED);
        Assert.assertEquals(taskService.findByName(userId, taskName).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        Assert.assertNull(taskService.findById(userId, taskId).getProjectId());
        Assert.assertNotNull(projectId);
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @NotNull final TaskDTO updTask = taskService.findById(userId, taskId);
        Assert.assertNotNull(updTask.getProjectId());
        Assert.assertEquals(updTask.getProjectId(), projectId);
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(projectId);
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        Assert.assertNull(taskService.findById(userId, taskId).getProjectId());
    }

    @Test
    public void findAllTasksByProjectId() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(projectId);
        @NotNull final List<TaskDTO> tasks = projectTaskService.findAllTasksByProjectId(userId, projectId);
        Assert.assertFalse(tasks.isEmpty());
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void removeProjectById() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(taskService.findById(userId, taskId).getProjectId());
        Assert.assertNotNull(projectId);
        projectTaskService.removeProjectById(userId, projectId);
        projectId = null;
        Assert.assertNull(taskService.findById(userId, taskId).getProjectId());
    }

    @Test
    public void removeProjectByIndex() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(taskService.findById(userId, taskId).getProjectId());
        projectTaskService.removeProjectByIndex(userId, 0);
        projectId = null;
        Assert.assertNull(taskService.findById(userId, taskId).getProjectId());
    }

    @Test
    public void removeProjectByName() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(taskService.findById(userId, taskId).getProjectId());
        projectTaskService.removeProjectByName(userId, "projectTest");
        projectId = null;
        Assert.assertNull(taskService.findById(userId, taskId).getProjectId());
    }

    @After
    public void finalizeTest() throws AbstractException {
        taskService.removeById(userId, taskId);
        if (projectId != null) projectService.removeById(userId, projectId);
        userService.removeById(userId);
    }

}
