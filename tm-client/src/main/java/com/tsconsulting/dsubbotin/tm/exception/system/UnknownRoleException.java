package com.tsconsulting.dsubbotin.tm.exception.system;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class UnknownRoleException extends AbstractException {

    public UnknownRoleException() {
        super("Incorrect role type entered!");
    }

}
