package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserLogOutCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-logout";
    }

    @Override
    @NotNull
    public String description() {
        return "User log out.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO session = endpointLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        endpointLocator.getSessionEndpoint().closeSession(session);
        endpointLocator.getSessionService().setSession(null);
        TerminalUtil.printMessage("Exit done.");
    }

}
