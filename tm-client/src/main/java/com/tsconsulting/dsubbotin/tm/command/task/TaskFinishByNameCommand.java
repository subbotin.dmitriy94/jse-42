package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Finish task by name.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().finishByNameTask(session, name);
        TerminalUtil.printMessage("[Updated task status]");
    }

}
