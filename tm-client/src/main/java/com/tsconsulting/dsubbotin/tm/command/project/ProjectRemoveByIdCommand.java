package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().findByIdProject(session, projectId);
        endpointLocator.getProjectTaskEndpoint().removeProjectById(session, projectId);
        TerminalUtil.printMessage("[Project removed]");
    }

}
